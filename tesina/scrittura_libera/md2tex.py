import markdown2
import pypandoc
import os
import shutil

def markdown_to_sections(markdown_file):
    with open(markdown_file, 'r', encoding='utf-8') as file:
        markdown_content = file.read()
    
    # Convert markdown to HTML
    faust_filter = os.path.join(os.getcwd(), 'faust_filter.py')
    html_content = markdown2.markdown(markdown_content)
    filters = ['/opt/homebrew/bin/python3.11', './faust_filter.py']
    # Convert HTML to LaTeX using pandoc
    latex_content = pypandoc.convert_text(html_content, 'latex', format='html', extra_args=['--filter=./faust_filter.py'])
    
    return latex_content

def save_sections_to_files(latex_content, output_dir):
    # Pulisce la cartella delle sezioni se esiste già
    if os.path.exists(output_dir):
        shutil.rmtree(output_dir)
    
    # Dividi il contenuto in sezioni
    sections = latex_content.split('\\section')
    
    os.makedirs(output_dir)
    
    all_tex_content = ''
    
    for i, section in enumerate(sections):
        if section.strip():
            section_filename = os.path.join(output_dir, f'section_{i+1}.tex')
            with open(section_filename, 'w', encoding='utf-8') as file:
                if i == 0:
                    file.write(section)
                else:
                    file.write('\\section' + section)
            
            print(f'Section {i+1} saved to {section_filename}')
            
            # Aggiungi l'input di questa sezione a all.tex
            all_tex_content += f'\\input{{sections/section_{i+1}}}\n'
    
    # Salva il contenuto di all.tex
    all_tex_filename = os.path.join(output_dir, 'all.tex')
    with open(all_tex_filename, 'w', encoding='utf-8') as all_tex_file:
        all_tex_file.write(all_tex_content)
    
    print(f'all.tex saved to {all_tex_filename}')

def main():
    markdown_file = './README.md'  # Nome del file Markdown di input
    output_dir = 'sections'   # Directory di output per i file LaTeX
    
    latex_content = markdown_to_sections(markdown_file)
    save_sections_to_files(latex_content, output_dir)
    print('Conversione completata!')

if __name__ == '__main__':
    main()
