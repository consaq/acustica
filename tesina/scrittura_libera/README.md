# Acustica

# Table of contents

## Caratteristiche campana tibetana
La campana tibetana è uno strumento a percussione proveniente dall'Himalaya. Non essendo appartenente al folklore europeo, ci sono pochi riferimenti in letteratura e scientifica e musicale. Ogni campana è generalmente costruita manualmente ed è composta da leghe particolari che insieme ad altri parametri fisici fondamentali come forma, dimensione e peso, ne modificano i modi di risonanza. Così ciascuna campana è differente dall'altra. Inoltre la sua costruzione prevede un'instabilità nel risultato spettrale ad ogni percussione, per cui di volta in volta viene accentuate alcune parziale in risposta alla risonanza dei modi che vengono eccitati. (nota articolo)

La campana che ho scelto io ha le seguenti caratteristiche: 

| Caratteristiche | Unità |
|-----------|-----------|
| Peso      | 550 g     |
| Materiali | -         |
| Altezza   | 5 cm      |
| Diametro  | 12 cm     |
| Spessore  | 2.5 mm    |

### Andamenti teorici della campana

#### Modi di vibrazione

Nel documento si descrivono tre regimi di moto fondamentali della campana in relazione alla forza normale (\(N_F\)) e alla velocità tangenziale (\(T_V\)):

1. **Vibrazione autoeccitata stabile con contatto permanente**: In questo regime, la campana vibra in modo continuo senza interruzioni nel contatto tra il puja e la campana. Si raggiunge una saturazione dell'ampiezza della vibrazione a causa di effetti non lineari. La maggior parte dell'energia è concentrata nel primo modo di vibrazione, con quattro nodi azimutali(citazione bilbao-b).

2. **Vibrazioni autoeccitate stabili con interruzioni periodiche del contatto**: In questo caso, il contatto tra il puja e la campana è periodicamente interrotto, risultando in una risposta spettrale con più energia alle alte frequenze. La forza di contatto può raggiungere valori massimi significativi, ma la risposta rimane quasi-periodica(citazione bilbao-b).

3. **Vibrazioni autoeccitate instabili con aumento intermittente dell'ampiezza**: Qui, il contatto tra il puja e la campana viene costantemente interrotto, portando a impatti caotici e una risposta sonora intermittente. Questo regime produce suoni curiosi, con una combinazione di caratteristiche "cantanti" e "risonanti" dovute a chattering caotico(citazione bilbao-b).

#### Energia liberata

L'energia liberata durante la vibrazione della campana dipende dal regime di moto:

- **Contatto permanente**: La maggior parte dell'energia è concentrata nel primo modo di vibrazione. Questo si traduce in una vibrazione stabile e continua fino a raggiungere la saturazione dovuta a effetti non lineari(citazione bilbao-b).

- **Interruzioni periodiche del contatto**: L'energia si distribuisce su un range più ampio di frequenze a causa delle interruzioni periodiche del contatto, con una porzione significativa dell'energia che si sposta verso le alte frequenze(citazione bilbao-b).

- **Aumento intermittente dell'ampiezza**: L'energia è caratterizzata da impatti caotici che causano un aumento e una diminuzione intermittente dell'ampiezza di vibrazione. Questo porta a una liberazione di energia meno regolare e più dispersiva rispetto agli altri regimi(citazione bilbao-b).

#### Simulazioni numeriche

Le simulazioni numeriche presentate nel documento utilizzano dati modali specifici per una campana con diametro di 152 mm e frequenza fondamentale di 314 Hz. Le simulazioni esplorano un range di forze normali (\(N_F\)) e velocità tangenziali (\(T_V\)), che includono condizioni tipiche di suonamento e impatti. Queste simulazioni aiutano a comprendere la risposta dinamica della campana sotto diverse condizioni di eccitazione(citazione bilbao-b).

#### Modelli di contatto e frizione

Nei modelli di simulazione, la rigidezza e dissipazione del contatto sono parametri cruciali. Ad esempio, una rigidezza del contatto di \(cK = 10^6\) N/m e una dissipazione di \(cC = 50\) Ns/m sono utilizzate per simulare il comportamento della campana. I parametri di frizione come \(S\mu = 0.4\) e \(D\mu = 0.2\) influenzano significativamente le risposte vibrazionali osservate nelle simulazioni(citazione bilbao-b).

Queste descrizioni teoriche e numeriche offrono una comprensione approfondita dei modi di vibrazione e dell'energia liberata dalle campane tibetane sotto diverse condizioni di eccitazione.

## Registrazioni

Poiché lo studio da me condotto ha interessi musicali, non ho avuto un atteggiamento statistico prendendo in esame più campane. Sono così passato direttamente allo studio fisico della campana da me scelta attraverso gli strumenti forniti dall' *Audio Content Analysis* in tempo differito, per poter poi approcciare all'*Audio Information Processing.*
Ho svolto delle registrazioni dei due principali gesti sonori svolti con l'ausilio della *puja* (il battente in legno ricoperto in feltro per metà).
Per osservare le possibili differenze dei modi di risonanza della campana ho registrato delle percussioni in un punto basso e in uno alto dello strumento.

Il tipo di registrazione scelta è una ripresa Ambisonics tradotta in MidSide per osservare il segnale diretto e quello riverberato su due diversi canali.
Il MidSide offre la possibilità di osservare il campo acustico a differenza del Left Right in cui le dimensioni fisiche del campo sono condensate (come un'equazione implicita). Ho utilizzato il microfono *H3-VR* della casa di produzione *ZOOM*. 

Di seguito una tabella dei vari eventi sonori registrati:

| Gesto       | Dinamica | N° files | orientamento |
|-------------|----------|----------|--------------|
| Impulsivo   | Forte    | 2        |  Dall'alto   |
| Impulsivo   | Forte    | 2        |  Dal basso   |
| Impulsivo   | Piano    | 2        |  Dall'alto   |
| Impulsivo   | Piano    | 2        |  Dal basso   |
| Strofinato  | Forte    | 1        |  -           |
| Strofinato  | Piano    | 1        |  -           |

l'impossibilità di ottenere dei risultati sempre simili mi ha portato ad utilizare almeno due differenti registrazione per osservare una media tra loro.
Di questi campioni sono state svolte le seguenti *Audio Content Analysis* attraverso le librerie *PyACA* e *MatPlotLib* di *Python3.11* e attraverso *Mathematica Wolfram Alpha*:
- Peak Time Envelope
- Waveform
- Spectral Slope
- Spectrogram

### Impulsivo Forte Dall'alto
**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f_ms_waveform.svg](docs/up_imp_f_ms_waveform.svg)
**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f2_ms_waveform.svg](docs/up_imp_f2_ms_waveform.svg)

**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f_ms_env.svg](docs/up_imp_f_ms_env.svg)
**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f2_ms_env.svg](docs/up_imp_f2_ms_env.svg)
**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f_spec_slope.svg](docs/up_imp_f_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f_spec.svg](docs/up_imp_f_spec.svg)
**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f2_spec.svg](docs/up_imp_f2_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f2_spece.svg](docs/up_imp_f2_spec.svg)


### Impulsivo Forte Dal basso
**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f_ms_waveform.svg](docs/up_imp_p_ms_waveform.svg)
**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f2_ms_waveform.svg](docs/up_imp_f2_ms_waveform.svg)

**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f_ms_env.svg](docs/up_imp_f_ms_env.svg)
**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f2_ms_env.svg](docs/up_imp_f2_ms_env.svg)

**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f_spec_slope.svg](docs/up_imp_f_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f_spec.svg](docs/up_imp_f_spec.svg)

**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f2_spec_slope.svg](docs/up_imp_f2_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_f2_spec.svg](docs/up_imp_f2_spec.svg)


### Impulsivo Piano Dall'alto

**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_p_ms_waveform.svg](docs/up_imp_p_ms_waveform.svg)

**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_p2_ms_waveform.svg](docs/up_imp_p2_ms_waveform.svg)

**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_p_ms_env.svg](docs/up_imp_p_ms_env.svg)

**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_p2_ms_env.svg](docs/up_imp_p2_ms_env.svg)

**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_p_spec_slope.svg](docs/up_imp_p_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_p_spec.svg](docs/up_imp_p_spec.svg)

**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_p2_spec_slope.svg](docs/up_imp_p2_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/up_imp_p2_spec.svg](docs/up_imp_p2_spec.svg)

### Impulsivo Piano Dal basso

**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/down_imp_p_ms_waveform.svg](docs/down_imp_p_ms_waveform.svg)
**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/down_imp_p2_ms_waveform.svg](docs/down_imp_p2_ms_waveform.svg)

**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/down_imp_p_ms_env.svg](docs/down_imp_p_ms_env.svg)
**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/down_imp_p2_ms_env.svg](docs/down_imp_p2_ms_env.svg)
**I campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/down_imp_p_spec_slope.svg](docs/down_imp_p_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/down_imp_p_spec.svg](docs/down_imp_p_spec.svg)
**II campione**
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/down_imp_p2_spec_slope.svg](docs/down_imp_p2_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/down_imp_p2_spec.svg](docs/down_imp_p2_spec.svg)



### Strofinato Piano

![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/strof_f_ms_waveform.svg](docs/strof_f_ms_waveform.svg)
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/strof_f_ms_env.svg](docs/strof_f_ms_env.svg)
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/strof_f_spec_slope.svg](docs/strof_f_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/strof_f_spec.svg](docs/strof_f_spec.svg)

### Strofinato Forte

![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/strof_p_ms_waveform.svg](docs/strof_p_ms_waveform.svg)
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/strof_p_ms_env.svg](docs/strof_p_ms_env.svg)
![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/strof_p_spec_slope.svg](docs/strof_p_spec_slope.svg)![https://gitlab.com/consaq/acustica/-/tree/main/tesina/scrittura_libera/docs/strof_p_spec.svg](docs/strof_p_spec.svg)



### Analisi e acquisizione di dati parametrici
L'analisi dati mi ha fornito la possibilità di creare descrittori e frequenziali e temporali:

#### Sonogramma
L'analisi attraverso il sonogramma, mi ha permesso di tracciare le frequenze delle prime 8 parziali:

| Parziali | Frequenza |
|----------|-----------|
| 1        | 713       |
| 2        | 1844      |
| 3        | 3284      |
| 4        | 4946      |
| 5        | 6788      |
| 6        | 7744      |
| 7        | 8769      |
| 8        | 10817     |

Utilizzando un filtro bandpass con un *ratio* molto stretto ho costruito un algoritmo che traccia l'inviluppo di ciascuna parziale. Questo diventa parametro di controllo per altri parametri per il DSP.

```faust
import("stdfaust.lib");

// Normalized Bandpass SVF TPT
BPTPTNormalized(gf, bw, cf, x) = loop ~ si.bus(2) : (! , ! , _ * gf)
    with {
        g = tan(cf * ma.PI * ma.T);
        R = 1.0 / (2.0 * bw);
        G1 = 1.0 / (1.0 + 2.0 * R * g + g * g);
        G2 = 2.0 * R + g;
        loop(s1, s2) = u1 , u2 , bp * 2.0 * R
            with {
                hp = (x - s1 * G2 - s2) * G1;
                v1 = hp * g;
                bp = s1 + v1;
                v2 = bp * g;
                lp = s2 + v2;
                u1 = v1 + bp;
                u2 = v2 + lp;
            };
    };

peakenvelope(period, x) = loop ~ _
    with {
        loop(y) = max(abs(x), y * coeff);
        twoPIforT = (2.0 * ma.PI) * (1.0 / ma.SR);
        coeff = exp(-twoPIforT / max(ma.EPSILON, period));
    };


// PeakHolder - holdTime in Seconds
peakHolder(holdTime, x) = loop ~ si.bus(2) : ! , _
with {
    loop(timerState, outState) = timer , output
    with {
        isNewPeak = abs(x) >= outState;
        isTimeOut = timerState >= (holdTime * ma.SR - 1);
        bypass = isNewPeak | isTimeOut;
        timer = ba.if(bypass, 0, timerState + 1);
        output = ba.if(bypass, abs(x), outState);
    };
};
/* picchi :
713
1844
3284
4946
6788
7744
8769
10817
*/
bpBank = _*4 <: peakHolder(1, BPTPTNormalized(10, 1000, 713)), peakHolder(1, BPTPTNormalized(10, 1000, 1844)), peakHolder(1, BPTPTNormalized(10, 1000, 3284)), peakHolder(1, BPTPTNormalized(10, 1000, 4946)), peakHolder(1, BPTPTNormalized(10, 1000, 6788)), peakHolder(1, BPTPTNormalized(10, 1000, 7744)), peakHolder(1, BPTPTNormalized(10, 1000, 8769)), peakHolder(1, BPTPTNormalized(10, 1000, 10817));

bpBank2 = _*4 <: peakenvelope(.1, BPTPTNormalized(10, 1000, 713)), peakenvelope(.1, BPTPTNormalized(10, 1000, 1844)), peakenvelope(.1, BPTPTNormalized(10, 1000, 3284)), peakenvelope(.1, BPTPTNormalized(10, 1000, 4946)), peakenvelope(.1, BPTPTNormalized(10, 1000, 6788)), peakenvelope(.1, BPTPTNormalized(10, 1000, 7744)), peakenvelope(.1, BPTPTNormalized(10, 1000, 8769)), peakenvelope(.1, BPTPTNormalized(10, 1000, 10817));

bpBank3 = _ <:  BPTPTNormalized(10, 1000, 713),  BPTPTNormalized(10, 1000, 1844), BPTPTNormalized(10, 1000, 3284), BPTPTNormalized(10, 1000, 4946),BPTPTNormalized(10, 1000, 6788), BPTPTNormalized(10, 1000, 7744),  BPTPTNormalized(10, 1000, 8769), BPTPTNormalized(10, 1000, 10817);



process = _ : bpBank ;
```
Il codice Faust fornito esegue una serie di operazioni partendo dal generale al particolare attraverso l'uso di diversi moduli e funzioni integrate in serie:

1. **BPTPTNormalized**: Questa funzione definisce un filtro passa-banda normalizzato utilizzando una topologia di filtro variabile di stato (SVF). Essa accetta parametri come guadagno (gf), larghezza di banda (bw), e frequenza centrale (cf), e applica il filtro al segnale in ingresso (x).

2. **peakenvelope**: Questa funzione implementa un envelope follower per rilevare i picchi del segnale. Utilizza un algoritmo che calcola il massimo del valore assoluto del segnale nel periodo specificato (period).

3. **peakHolder**: Questa funzione implementa un meccanismo di "peak holding", che trattiene il valore massimo di un picco per un certo periodo di tempo (holdTime). Questo è utile per mantenere e manipolare i picchi rilevati nel segnale.

4. **bpBank, bpBank2**: Queste variabili rappresentano una serie di filtri e operazioni applicate in sequenza al segnale in ingresso. `bpBank` applica il `peakHolder` ai filtri passa-banda normalizzati, mentre `bpBank2` utilizza l'envelope follower `peakenvelope` su questi stessi filtri.

5. **process**: Infine, la variabile `process` unisce il segnale in ingresso ad `bpBank`, eseguendo quindi tutte le operazioni di filtraggio e rilevamento dei picchi definite precedentemente.

Questo approccio in cascata permette di ottenere un controllo dettagliato e specifico sulla manipolazione del segnale audio o del segnale trattato dall'algoritmo Faust.

#### Spectral slope
Lo spectral slope (pendenza spettrale) è un parametro audio che fornisce informazioni sulla forma complessiva dello spettro del segnale fornito. È una misura che descrive come l'energia spettrale cambia con la frequenza. Nella libreria PyACA (Python Audio Content Analysis), la funzione per calcolare lo spectral slope permette di analizzare la distribuzione delle frequenze in un segnale audio.
**Definizione:** Lo spectral slope è la pendenza della linea di regressione lineare che meglio approssima l'andamento dello spettro di un segnale audio in una scala logaritmica.
**Calcolo:** Viene calcolato come il coefficiente angolare della retta che meglio approssima l'andamento dello spettro di frequenza del segnale in uno spazio logaritmico.
**Uso:** Questa misura è utile per distinguere tra suoni con contenuti armonici differenti. Per esempio, suoni più brillanti o acuti avranno uno spectral slope più ripido rispetto a suoni più scuri o bassi.

##### Procedura di Calcolo
**Analisi di Fourier:** Il segnale audio viene trasformato dal dominio del tempo al dominio della frequenza utilizzando la Trasformata di Fourier.
**Logaritmizzazione:** Le ampiezze delle componenti spettrali vengono convertite in scala logaritmica per ottenere una rappresentazione più lineare delle variazioni.
**Regressione Lineare:** Viene eseguita una regressione lineare sulle ampiezze logaritmizzate in funzione delle frequenze (anch'esse in scala logaritmica).
**Pendenza:** La pendenza della retta ottenuta dalla regressione lineare rappresenta lo spectral slope.


Dallo SpectralSlope ho potuto costruire un grafico logaritmico dove osservare il decadimento delle parziali in base alla loro frequenza. (Interessante come la 2° parziale cada in breve tempo rispetto alle altre)

Il *peakEnvelope* e la *Waveform* mi permettono di stabilire quale sia il tempo di integrazione del filtro che costituisce *l'Envelope Follower*. Dopo un attenta analisi ho compreso come il tempo di integrazione migliore è 0.1 secondi. Questo tempo è fondamentale per tracciare un inviluppo coerente alla realtà fisica della campana prevenendo errori di sensibilità dello strumento  (con un tempo maggiore si ha una perdita di informazione sulla pendenza dinamica, mentre con uno minore compaiono *ripples*).

### Bibliografia
- [bilbao03_ams004a](https://gitlab.com/consaq/acustica/-/blob/main/tesina/scrittura_libera/refs/Bilbao03_ams004b.pdf?ref_type=heads)
- [bilbao03_ams004b](https://gitlab.com/consaq/acustica/-/blob/main/tesina/scrittura_libera/refs/Bilbao03_ams004b.pdf?ref_type=heads)
- [Park-Princeton Thesis 2004](https://gitlab.com/consaq/acustica/-/blob/main/tesina/scrittura_libera/refs/Park-Princeton%20Thesis%202004.pdf?ref_type=heads)
- [TibetanSingingBowls](https://gitlab.com/consaq/acustica/-/blob/main/tesina/scrittura_libera/refs/TibetanSingingBowls.pdf?ref_type=heads)


# Sitografia
- [pyACA](https://github.com/alexanderlerch/pyACA)
- [Python 3.11 ](https://www.python.org/downloads/release/python-3110/)
- dipendencies di python: [Matplotlib](https://matplotlib.org/), [NumPy](https://numpy.org/), [ScyPy](https://scipy.org/)
- [https://gitlab.com/consaq/acustica](https://gitlab.com/consaq/acustica)
- [ZOOM VR-H3](https://zoomcorp.com/it/it/handheld-recorders/registratori-palmari/h3-vr-360-audio-recorder/)