\section{Caratteristiche campana
tibetana}\label{caratteristiche-campana-tibetana}

La campana tibetana è uno strumento a percussione proveniente
dall\textquotesingle Himalaya. Non essendo appartenente al folklore
europeo, ci sono pochi riferimenti in letteratura e scientifica e
musicale. Ogni campana è generalmente costruita manualmente ed è
composta da leghe particolari che insieme ad altri parametri fisici
fondamentali come forma, dimensione e peso, ne modificano i modi di
risonanza. Così ciascuna campana è differente
dall\textquotesingle altra. Inoltre la sua costruzione prevede
un\textquotesingle instabilità nel risultato spettrale ad ogni
percussione, per cui di volta in volta viene accentuate alcune parziale
in risposta alla risonanza dei modi che vengono eccitati\cite{terwagne2011tibetan}.

La campana che ho scelto io ha le seguenti caratteristiche:

\begin{table}[h!]
  \centering
  \begin{tabular}{| l | l |}
      \hline
      \textbf{Caratteristiche} & \textbf{Unità} \\
      \hline
      Peso      & 550 g     \\
      \hline
      Materiali & -         \\
      \hline
      Altezza   & 5 cm      \\
      \hline
      Diametro  & 12 cm     \\
      \hline
      Spessore  & 2.5 mm    \\
      \hline
  \end{tabular}
  \caption{Tabella delle caratteristiche}
  \label{tab:caratteristiche}
\end{table}

\section{Andamenti teorici della
campana}\label{andamenti-teorici-della-campana}

\paragraph{Modi di vibrazione}\label{modi-di-vibrazione}

Nel documento \cite{inaciophysics} si descrivono tre regimi di moto fondamentali della
campana in relazione alla forza normale ((N\emph{F)) e alla velocità
tangenziale ((T}V)):

\begin{enumerate}
\item
  \textbf{Vibrazione autoeccitata stabile con contatto permanente}: In
  questo regime, la campana vibra in modo continuo senza interruzioni
  nel contatto tra il puja e la campana. Si raggiunge una saturazione
  dell\textquotesingle ampiezza della vibrazione a causa di effetti non
  lineari. La maggior parte dell\textquotesingle energia è concentrata
  nel primo modo di vibrazione, con quattro nodi azimutali\cite{inaciophysics}.
\item
  \textbf{Vibrazioni autoeccitate stabili con interruzioni periodiche
  del contatto}: In questo caso, il contatto tra il puja e la campana è
  periodicamente interrotto, risultando in una risposta spettrale con
  più energia alle alte frequenze. La forza di contatto può raggiungere
  valori massimi significativi, ma la risposta rimane
  quasi-periodica \cite{inaciophysics}.
\item
  \textbf{Vibrazioni autoeccitate instabili con aumento intermittente
  dell\textquotesingle ampiezza}: Qui, il contatto tra il puja e la
  campana viene costantemente interrotto, portando a impatti caotici e
  una risposta sonora intermittente. Questo regime produce suoni
  curiosi, con una combinazione di caratteristiche "cantanti" e
  "risonanti" dovute a chattering caotico \cite{inaciophysics}.
\end{enumerate}

\paragraph{Energia liberata}\label{energia-liberata}

L\textquotesingle energia liberata durante la vibrazione della campana
dipende dal regime di moto:

\begin{itemize}
\item
  \textbf{Contatto permanente}: La maggior parte
  dell\textquotesingle energia è concentrata nel primo modo di
  vibrazione. Questo si traduce in una vibrazione stabile e continua
  fino a raggiungere la saturazione dovuta a effetti non
  lineari(citazione bilbao-b).
\item
  \textbf{Interruzioni periodiche del contatto}:
  L\textquotesingle energia si distribuisce su un range più ampio di
  frequenze a causa delle interruzioni periodiche del contatto, con una
  porzione significativa dell\textquotesingle energia che si sposta
  verso le alte frequenze(citazione bilbao-b).
\item
  \textbf{Aumento intermittente dell\textquotesingle ampiezza}:
  L\textquotesingle energia è caratterizzata da impatti caotici che
  causano un aumento e una diminuzione intermittente
  dell\textquotesingle ampiezza di vibrazione. Questo porta a una
  liberazione di energia meno regolare e più dispersiva rispetto agli
  altri regimi(citazione bilbao-b).
\end{itemize}

\paragraph{Simulazioni numeriche}\label{simulazioni-numeriche}

Le simulazioni numeriche presentate nel documento utilizzano dati modali
specifici per una campana con diametro di 152 mm e frequenza
fondamentale di 314 Hz. Le simulazioni esplorano un range di forze
normali ((N\emph{F)) e velocità tangenziali ((T}V)), che includono
condizioni tipiche di suonamento e impatti. Queste simulazioni aiutano a
comprendere la risposta dinamica della campana sotto diverse condizioni
di eccitazione(citazione bilbao-b).

\paragraph{Modelli di contatto e
frizione}\label{modelli-di-contatto-e-frizione}

Nei modelli di simulazione, la rigidezza e dissipazione del contatto
sono parametri cruciali. Ad esempio, una rigidezza del contatto di (cK =
10\^{}6) N/m e una dissipazione di (cC = 50) Ns/m sono utilizzate per
simulare il comportamento della campana. I parametri di frizione come
(S\textbackslash mu = 0.4) e (D\textbackslash mu = 0.2) influenzano
significativamente le risposte vibrazionali osservate nelle
simulazioni(citazione bilbao-b).

Queste descrizioni teoriche e numeriche offrono una comprensione
approfondita dei modi di vibrazione e dell\textquotesingle energia
liberata dalle campane tibetane sotto diverse condizioni di eccitazione\cite{inaciophysics}.

\subsection{Registrazioni}\label{registrazioni}

Poiché lo studio da me condotto ha interessi musicali, non ho avuto un
atteggiamento statistico prendendo in esame più campane. Sono così
passato direttamente allo studio fisico della campana da me scelta
attraverso gli strumenti forniti dall\textquotesingle{} \emph{Audio
Content Analysis} in tempo differito, per poter poi approcciare
all\textquotesingle{}\emph{Audio Information Processing.} Ho svolto
delle registrazioni dei due principali gesti sonori svolti con
l\textquotesingle ausilio della \emph{puja} (il battente in legno
ricoperto in feltro per metà). Per osservare le possibili differenze dei
modi di risonanza della campana ho registrato delle percussioni in un
punto basso e in uno alto dello strumento.

Il tipo di registrazione scelta è una ripresa Ambisonics tradotta in
MidSide per osservare il segnale diretto e quello riverberato su due
diversi canali. Il MidSide offre la possibilità di osservare il campo
acustico a differenza del Left Right in cui le dimensioni fisiche del
campo sono condensate (come un\textquotesingle equazione implicita). Ho
utilizzato il microfono \emph{H3-VR} della casa di produzione
\emph{ZOOM}.

Di seguito una tabella dei vari eventi sonori registrati:

\begin{table}[h]
  \centering
  \begin{tabular}{|l|l|c|l|}
      \hline
      \textbf{Gesto} & \textbf{Dinamica} & \textbf{N° files} & \textbf{Orientamento} \\
      \hline
      Impulsivo   & Forte    & 2        & Dall'alto   \\
      \hline
      Impulsivo   & Forte    & 2        & Dal basso   \\
      \hline
      Impulsivo   & Piano    & 2        & Dall'alto   \\
      \hline
      Impulsivo   & Piano    & 2        & Dal basso   \\
      \hline
      Strofinato  & Forte    & 1        & -           \\
      \hline
      Strofinato  & Piano    & 1        & -           \\
      \hline
  \end{tabular}
  \caption{Tabella dei gesti e delle dinamiche}
  \label{tab:gesti-dinamiche}
\end{table}

l\textquotesingle impossibilità di ottenere dei risultati sempre simili
mi ha portato ad utilizare almeno due differenti registrazione per
osservare una media tra loro. Di questi campioni sono state svolte le
seguenti \emph{Audio Content Analysis} attraverso le librerie
\emph{PyACA} e \emph{MatPlotLib} di \emph{Python3.11} e attraverso
\emph{Mathematica Wolfram Alpha}: 
\begin{itemize}
\item{Peak Time Envelope}
\item{Waveform}
\item{Spectral Slope }
\item{Spectrogram}
\end{itemize}


\subsubsection{Impulsivo Forte
Dall\textquotesingle alto}\label{impulsivo-forte-dallalto}

\textbf{I campione} 

\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_f_ms_waveform.svg}
\end{figure}
\newpage

\textbf{II campione}

\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_f2_ms_waveform.svg}
\end{figure}

\textbf{I campione} 

\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_f_ms_env.svg}
\end{figure}

\textbf{II campione} 

\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_f2_ms_env.svg}
\end{figure}

\newpage

\textbf{I campione}

\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_f_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_f_spec.svg}
  \end{subfigure}
\end{figure}




\textbf{II campione}

\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_f2_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_f2_spec.svg}
  \end{subfigure}
\end{figure}

\subsubsection{Impulsivo Forte Dal
basso}\label{impulsivo-forte-dal-basso}

\textbf{I campione} 
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_p_ms_waveform.svg}
\end{figure}

\textbf{II campione} 
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_f2_ms_waveform.svg}
\end{figure}

\newpage
\textbf{I campione} 
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_f_ms_env.svg}
\end{figure}


\textbf{II campione} 
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_f2_ms_env.svg}
\end{figure}

\newpage
\textbf{I campione}

\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_f_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_f_spec.svg}
  \end{subfigure}
\end{figure}




\textbf{II campione}

\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_f2_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_f2_spec.svg}
  \end{subfigure}
\end{figure}




\subsubsection{Impulsivo Piano
Dall\textquotesingle alto}\label{impulsivo-piano-dallalto}


\textbf{I campione} 
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_p_ms_waveform.svg}
\end{figure}

\textbf{II campione}
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_p2_ms_waveform.svg}
\end{figure}
\newpage
\textbf{I campione} 
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_p_ms_env.svg}
\end{figure}

\textbf{II campione}
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/up_imp_p2_ms_env.svg}
\end{figure}
\newpage
\textbf{I campione}
\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_p_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_p_spec.svg}
  \end{subfigure}
\end{figure}


\textbf{II campione}
\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_p2_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/up_imp_p2_spec.svg}
  \end{subfigure}
\end{figure}


\subsubsection{Impulsivo Piano Dal
basso}\label{impulsivo-piano-dal-basso}

\textbf{I campione}
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/down_imp_p_ms_waveform.svg}
\end{figure}

\textbf{II campione} 
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/down_imp_p2_ms_waveform.svg}
\end{figure}

\newpage
\textbf{I campione}
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/down_imp_p_ms_env.svg}
\end{figure}

\textbf{II campione}
\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/down_imp_p2_ms_env.svg}
\end{figure}

\newpage

\textbf{I campione}
\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/down_imp_p_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/down_imp_p_spec.svg}
  \end{subfigure}
\end{figure}

\textbf{II campione}
\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/down_imp_p2_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/down_imp_p2_spec.svg}
  \end{subfigure}
\end{figure}


\subsubsection{Strofinato Forte}\label{strofinato-forte}

\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/strof_f_ms_waveform.svg}
\end{figure}


\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/strof_f_ms_env.svg}
\end{figure}

\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/strof_f_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/strof_f_spec.svg}
  \end{subfigure}
\end{figure}
\newpage
\subsubsection{Strofinato Piano}\label{strofinato-piano}

\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/strof_p_ms_waveform.svg}
\end{figure}


\begin{figure}[h]
  \centering
\includesvg[width=1\linewidth]{./docs/strof_p_ms_env.svg}
\end{figure}

\begin{figure}[h!]
  \centering
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/strof_p_spec_slope.svg}
  \end{subfigure}
  \begin{subfigure}{0.4\linewidth}
    \includesvg[width=\linewidth]{./docs/strof_p_spec.svg}
  \end{subfigure}
\end{figure}

\newpage
\subsection{Analisi e acquisizione di dati
parametrici}\label{analisi-e-acquisizione-di-dati-parametrici}

L\textquotesingle analisi dati mi ha fornito la possibilità di creare
descrittori e frequenziali e temporali:

\paragraph{Sonogramma}\label{sonogramma}

L\textquotesingle analisi attraverso il sonogramma, mi ha permesso di
tracciare le frequenze delle prime 8 parziali:

\begin{table}[h!]
    \centering
    \begin{tabular}{|c|c|}
        \hline
        \textbf{Parziali} & \textbf{Frequenza} \\
        \hline
        1    & 713   \\
        \hline
        2    & 1844  \\
        \hline
        3    & 3284  \\
        \hline
        4    & 4946  \\
        \hline
        5    & 6788  \\
        \hline
        6    & 7744  \\
        \hline
        7    & 8769  \\
        \hline
        8    & 10817 \\
        \hline
    \end{tabular}
    \caption{Tabella delle Frequenze per i Parziali}
    \label{tab:frequenze_parziali}
\end{table}


Utilizzando un filtro bandpass con un \emph{ratio} molto stretto ho
costruito un algoritmo che traccia l\textquotesingle inviluppo di
ciascuna parziale. Questo diventa parametro di controllo per altri
parametri per il DSP.


  \begin{lstlisting}
    import("stdfaust.lib");

    // Normalized Bandpass SVF TPT
    BPTPTNormalized(gf, bw, cf, x) = loop ~ si.bus(2) : (! , ! , _ * gf)
        with {
            g = tan(cf * ma.PI * ma.T);
            R = 1.0 / (2.0 * bw);
            G1 = 1.0 / (1.0 + 2.0 * R * g + g * g);
            G2 = 2.0 * R + g;
            loop(s1, s2) = u1 , u2 , bp * 2.0 * R
                with {
                    hp = (x - s1 * G2 - s2) * G1;
                    v1 = hp * g;
                    bp = s1 + v1;
                    v2 = bp * g;
                    lp = s2 + v2;
                    u1 = v1 + bp;
                    u2 = v2 + lp;
                };
        };
    
    peakenvelope(period, x) = loop ~ _
        with {
            loop(y) = max(abs(x), y * coeff);
            twoPIforT = (2.0 * ma.PI) * (1.0 / ma.SR);
            coeff = exp(-twoPIforT / max(ma.EPSILON, period));
        };
    
    
    // PeakHolder - holdTime in Seconds
    peakHolder(holdTime, x) = loop ~ si.bus(2) : ! , _
    with {
        loop(timerState, outState) = timer , output
        with {
            isNewPeak = abs(x) >= outState;
            isTimeOut = timerState >= (holdTime * ma.SR - 1);
            bypass = isNewPeak | isTimeOut;
            timer = ba.if(bypass, 0, timerState + 1);
            output = ba.if(bypass, abs(x), outState);
        };
    };
    /* picchi :
    713
    1844
    3284
    4946
    6788
    7744
    8769
    10817
    */
    bpBank = _*4 <: peakHolder(1, BPTPTNormalized(10, 1000, 713)), peakHolder(1, BPTPTNormalized(10, 1000, 1844)), peakHolder(1, BPTPTNormalized(10, 1000, 3284)), peakHolder(1, BPTPTNormalized(10, 1000, 4946)), peakHolder(1, BPTPTNormalized(10, 1000, 6788)), peakHolder(1, BPTPTNormalized(10, 1000, 7744)), peakHolder(1, BPTPTNormalized(10, 1000, 8769)), peakHolder(1, BPTPTNormalized(10, 1000, 10817));
    
    bpBank2 = _*4 <: peakenvelope(.1, BPTPTNormalized(10, 1000, 713)), peakenvelope(.1, BPTPTNormalized(10, 1000, 1844)), peakenvelope(.1, BPTPTNormalized(10, 1000, 3284)), peakenvelope(.1, BPTPTNormalized(10, 1000, 4946)), peakenvelope(.1, BPTPTNormalized(10, 1000, 6788)), peakenvelope(.1, BPTPTNormalized(10, 1000, 7744)), peakenvelope(.1, BPTPTNormalized(10, 1000, 8769)), peakenvelope(.1, BPTPTNormalized(10, 1000, 10817));
    
    bpBank3 = _ <:  BPTPTNormalized(10, 1000, 713),  BPTPTNormalized(10, 1000, 1844), BPTPTNormalized(10, 1000, 3284), BPTPTNormalized(10, 1000, 4946),BPTPTNormalized(10, 1000, 6788), BPTPTNormalized(10, 1000, 7744),  BPTPTNormalized(10, 1000, 8769), BPTPTNormalized(10, 1000, 10817);
    
    
    
    process = _ : bpBank ;
  \end{lstlisting}

  Il codice Faust fornito esegue una serie di
operazioni partendo dal generale al particolare attraverso
l\textquotesingle uso di diversi moduli e funzioni integrate in serie:

\begin{enumerate}
\item
  \textbf{BPTPTNormalized}: Questa funzione definisce un filtro
  passa-banda normalizzato utilizzando una topologia di filtro variabile
  di stato \texttt{SVF}. Essa accetta parametri come guadagno \texttt{gf}, larghezza
  di banda \texttt{bw}, e frequenza centrale \texttt{cf}, e applica il filtro al
  segnale in ingresso \texttt{x}.
\item
  \textbf{peakenvelope}: Questa funzione implementa un envelope follower
  per rilevare i picchi del segnale. Utilizza un algoritmo che calcola
  il massimo del valore assoluto del segnale nel periodo specificato
  (period).
\item
  \textbf{peakHolder}: Questa funzione implementa un meccanismo di "peak
  holding", che trattiene il valore massimo di un picco per un certo
  periodo di tempo (holdTime). Questo è utile per mantenere e manipolare
  i picchi rilevati nel segnale.
\item
  \textbf{bpBank, bpBank2}: Queste variabili rappresentano una serie di
  filtri e operazioni applicate in sequenza al segnale in ingresso.
  \texttt{bpBank} applica il \texttt{peakHolder} ai filtri passa-banda
  normalizzati, mentre \texttt{bpBank2} utilizza
  l\textquotesingle envelope follower \texttt{peakenvelope} su questi
  stessi filtri.
\item
  \textbf{process}: Infine, la variabile \texttt{process} unisce il
  segnale in ingresso ad \texttt{bpBank}, eseguendo quindi tutte le
  operazioni di filtraggio e rilevamento dei picchi definite
  precedentemente.
\end{enumerate}

Questo approccio in cascata permette di ottenere un controllo
dettagliato e specifico sulla manipolazione del segnale audio o del
segnale trattato dall\textquotesingle algoritmo Faust.

\subsection{Descrittori}\label{descrittori}

\paragraph{Spectral slope}\label{spectral-slope}

Lo spectral slope (pendenza spettrale) è un parametro audio che fornisce
informazioni sulla forma complessiva dello spettro del segnale fornito.
È una misura che descrive come l\textquotesingle energia spettrale
cambia con la frequenza\cite{park2004towards}. Nella libreria PyACA (Python Audio Content
Analysis), la funzione per calcolare lo spectral slope permette di
analizzare la distribuzione delle frequenze in un segnale audio.
\textbf{Definizione:} Lo spectral slope è la pendenza della linea di
regressione lineare che meglio approssima l\textquotesingle andamento
dello spettro di un segnale audio in una scala logaritmica.
\textbf{Calcolo:} Viene calcolato come il coefficiente angolare della
retta che meglio approssima l\textquotesingle andamento dello spettro di
frequenza del segnale in uno spazio logaritmico. \textbf{Uso:} Questa
misura è utile per distinguere tra suoni con contenuti armonici
differenti. Per esempio, suoni più brillanti o acuti avranno uno
spectral slope più ripido rispetto a suoni più scuri o bassi.
Procedura di Calcolo:

\begin{itemize}
  \item{
\textbf{Analisi di Fourier:} Il segnale audio viene trasformato dal
dominio del tempo al dominio della frequenza utilizzando la Trasformata
di Fourier.}
\item{\textbf{Logaritmizzazione:} Le ampiezze delle componenti
spettrali vengono convertite in scala logaritmica per ottenere una
rappresentazione più lineare delle variazioni.}
\item{\textbf{Regressione
Lineare:} Viene eseguita una regressione lineare sulle ampiezze
logaritmizzate in funzione delle frequenze (anch\textquotesingle esse in
scala logaritmica).}
\item{\textbf{Pendenza:} La pendenza della retta ottenuta
dalla regressione lineare rappresenta lo spectral slope.}
\end{itemize}
Dallo SpectralSlope ho potuto costruire un grafico logaritmico dove
osservare il decadimento delle parziali in base alla loro frequenza.
(Interessante come la 2° parziale cada in breve tempo rispetto alle
altre)

Il \emph{peakEnvelope} e la \emph{Waveform} mi permettono di stabilire
quale sia il tempo di integrazione del filtro che costituisce
\emph{l\textquotesingle Envelope Follower}. Dopo un attenta analisi ho
compreso come il tempo di integrazione migliore è 0.1 secondi. Questo
tempo è fondamentale per tracciare un inviluppo coerente alla realtà
fisica della campana prevenendo errori di sensibilità dello strumento
(con un tempo maggiore si ha una perdita di informazione sulla pendenza
dinamica, mentre con uno minore compaiono \emph{ripples}).