import os
import matplotlib.pyplot as plt
import numpy as np
import pyACA
from pyACA.computeFeature import computeFeature
from pyACA.ToolReadAudio import ToolReadAudio
from scipy.io import wavfile  # Importa wavfile da scipy.io



def plot_spectral_slope(audio, fs, output_file):
    spectral_slope, freq_vector = computeFeature('SpectralSlope', audio, fs)

    plt.figure(figsize=(5, 2))
    plt.plot(freq_vector, spectral_slope)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Spectral Slope (dB/oct)')
    plt.title('Spectral Slope')
    plt.grid(True)
    plt.savefig(output_file)
    plt.close()

def create_mid_side_channels(audio):
    left_channel = audio[:, 0]
    right_channel = audio[:, 1]

    # Mid channel (sum)
    mid_channel = (left_channel + right_channel) / np.sqrt(2)

    # Side channel (difference)
    side_channel = (left_channel - right_channel) / np.sqrt(2)

    return mid_channel, side_channel


def plot_waveform(fs, output_dir, filename, mid_channel, side_channel):
    plt.figure(figsize=(12, 4))  # Increase the height of the figure
    time = np.arange(len(mid_channel)) / fs

    # Plot Mid channel
    plt.subplot(1, 2, 1)
    plt.plot(time, mid_channel, color='purple')
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')
    plt.title('Mid Channel Waveform')
    plt.grid(True)

    # Plot Side channel
    plt.subplot(1, 2, 2)
    plt.plot(time, side_channel, color='orange')
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')
    plt.title('Side Channel Waveform')
    plt.grid(True)

    # Save the figure
    channel_file = os.path.join(output_dir, f'{os.path.splitext(filename)[0]}_ms_waveform.svg')
    plt.savefig(channel_file)
    plt.close()

def plot_time_peak_envelope(fs, output_dir, filename, mid_channel, side_channel):
    mid_peak_envelope, time_vector = computeFeature('TimePeakEnvelope', mid_channel, fs)
    side_peak_envelope, _ = computeFeature('TimePeakEnvelope', side_channel, fs)

    plt.figure(figsize=(12, 4))

    # Plot Mid Channel Time Peak Envelope
    plt.subplot(1, 2, 1)
    plt.plot(time_vector, mid_peak_envelope[0], color='purple')
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')
    plt.title('Mid Channel Time Peak Envelope')
    plt.grid(True)

    # Plot Side Channel Time Peak Envelope
    plt.subplot(1, 2, 2)
    plt.plot(time_vector, side_peak_envelope[0], color='orange')
    plt.xlabel('Time (s)')
    plt.ylabel('Amplitude')
    plt.title('Side Channel Time Peak Envelope')
    plt.grid(True)

    # Save the figure
    channel_file = os.path.join(output_dir, f'{os.path.splitext(filename)[0]}_ms_env.svg')
    plt.savefig(channel_file)
    plt.close()



def process_audio_files(input_dir, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    for filename in os.listdir(input_dir):
        if filename.lower().endswith('.wav'):
            file_path = os.path.join(input_dir, filename)
            a = ToolReadAudio(file_path)
            # Prendi solo i primi 45 secondi
            fs = a[0]
            audio = a[1]
            num_samples = int(45 * fs)
            if audio.shape[0] > num_samples:
                audio = audio[:num_samples, :]
 
            base_name = os.path.splitext(filename)[0]
            spectral_slope_file = os.path.join(output_dir, f'{base_name}_spec_slope.svg')
            spectrogram_file = os.path.join(output_dir, f'{base_name}_spectrogram.svg')

            mid_channel, side_channel = create_mid_side_channels(audio)

                #plot
            plot_waveform(fs, output_dir, filename, mid_channel, side_channel)
            plot_spectral_slope(audio,fs, spectral_slope_file)
            plot_time_peak_envelope(fs, output_dir, filename, mid_channel, side_channel)

# Example usage
input_directory = '../inputAnal'  # Replace with your input directory
output_directory = '../docs'      # Replace with your output directory
process_audio_files(input_directory, output_directory)