# Acustica

# Table of contents

## Caratteristiche campana tibetana
- peso
- materiali
- altezza, diametro, spessore
- modalità di diffusione e microfonazione (ampiezza dell'onda)
### Registrazioni
- percussione dal basso e dall'alto
- strofinato pressione e velocità elevata, pressione e velocità minore 
- spiegazione della dimensione empirica del risultato:
	- differenze nei tempi di attacco e di rilascio nella pendenza nell'ampiezza dell'onda; 
	- inviluppo spettrale;
	- plot dei sonogrammi;



### Considerazioni per le scelte artistiche e compositive
Decisione sulle scelte del brano in funzione dei risultati.

### Envelope follower
In relazione alla modalità d'attacco e ai tempi

### Bibliografia
- bilbao 1
- bilbao 2