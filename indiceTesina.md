# Tesina

- Abstract

- Introduzione
   1. dichiarazione intenti
   2. descrizione sintetica metodo di lavoro

- Svolgimento
   1. campana tibetana
     1.1 descrizione modi di vibrazione e inviluppo spettrale
     1.2 propagazione propria della campana
   2. sistema feedback
     2.1 metodi di eccitazione rapportati al sistema
     2.2 metodi di eccitazione rapportati allo spazio
   3. uso dei descrittori
     3.1 descr. Scelti e perché

- Conclusione
- Bibliografia